import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SurveyListComponent } from './survey-list.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: SurveyListComponent
  }
]

@NgModule({
  declarations: [SurveyListComponent], //Vet ej om denna ska bort
  imports: [ CommonModule, RouterModule.forChild( routes ) ],
  exports: [ RouterModule ]
})
export class SurveyListModule { }