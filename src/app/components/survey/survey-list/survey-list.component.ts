import { Component, OnInit } from '@angular/core';
import { SurveyService } from 'src/app/services/survey/survey.service';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  title = 'angular-FEdev';
  public surveys: any[] = [];

  constructor(private survey: SurveyService) { 
    this.displaySurveys();
  }

  ngOnInit(): void {
  }

  async displaySurveys() {
    try {
      const result: any = await this.survey.getSurveys();
      if (result.status < 400) {
        this.surveys = result.data;
      }
    } catch (e) {

    }

  }

}
