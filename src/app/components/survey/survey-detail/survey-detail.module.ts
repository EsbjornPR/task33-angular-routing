import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyDetailComponent } from './survey-detail.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: SurveyDetailComponent
  }
]

@NgModule({
  declarations: [SurveyDetailComponent], //Vet ej om denna ska bort
  imports: [ CommonModule, RouterModule.forChild( routes ) ],
  exports: [ RouterModule ]
})
export class SurveyDetailModule { }
