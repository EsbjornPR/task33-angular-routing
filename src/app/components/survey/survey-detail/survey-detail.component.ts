import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SurveyService } from 'src/app/services/survey/survey.service';

@Component({
  selector: 'app-survey-detail',
  templateUrl: './survey-detail.component.html',
  styleUrls: ['./survey-detail.component.css']
})
export class SurveyDetailComponent implements OnInit {
  public requestedSurveyId: string = '';
  public surveyDetails: any = {};

  constructor(private route: ActivatedRoute, private survey: SurveyService) { 
    this.requestedSurveyId = this.route.snapshot.paramMap.get('id');
    this.dispaySurvey();
  }

  ngOnInit(): void {
  }

  async dispaySurvey() {
    try {
      const result: any = await this.survey.getSurvey(this.requestedSurveyId);
      if (result.status < 400) {
        this.surveyDetails = result.data;
      }
    } catch (e) {
      
    }
  }

}
