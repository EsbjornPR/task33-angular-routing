import { Component } from '@angular/core';
import { SessionService } from './services/session/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-fe-dev';
  loggedIn: boolean = false;

  constructor(private session: SessionService) {

    if (this.session.get() !== false) {
      this.loggedIn = true;
    }

  }


}


