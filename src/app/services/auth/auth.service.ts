import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }


  // Using environment variables
  register(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
      user: { ...user}
    }).toPromise();
  }

  // Not using environment variables
  login(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/login', {
      user: { ...user}
    }).toPromise();
  }


}



