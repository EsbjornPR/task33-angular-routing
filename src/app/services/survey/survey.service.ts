import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session/session.service';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  constructor(private http: HttpClient, private session: SessionService) { }


  // Using environment variables
  register(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
      user: { ...user }
    }).toPromise();
  }

  getSurveys(): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/surveys`, {
      headers: {
        'Authorization': 'Bearer '+ this.session.get().token
      }
    }).toPromise();
  }

  getSurvey(id: string): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/surveys/${id}`, {
      headers: {
        'Authorization': 'Bearer '+ this.session.get().token
      }
    }).toPromise();
  }
  
}
