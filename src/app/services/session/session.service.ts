import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  // save(token: string) {
  //   localStorage.setItem('sp_jwt', token);
  // }

  save(session: any) {
    localStorage.setItem('sp_session', JSON.stringify(session));
  }

  // get(): string {
  //   return localStorage.getItem('sp_jwt') || ''; // Returns a empty string if the token is not found
  // }

  get(): any {
    const savedSession = localStorage.getItem('sp_session');
    return savedSession ? JSON.parse(savedSession) : false; // If saved session exists. parse converts from json string to Javascript object
  }
}
